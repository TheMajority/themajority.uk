# Preventing Environment Catastrophe requires Fair voting systems

## *["Proportional Representation is not just better for democracy, but has associated positive outcomes relating to the environment"][1]*

### [Stability](Stable) is needed to save us

#### Any effective investment must be long term
##### There is no way to fix the Environment with short term investment

#### No country can fix the global climate alone
##### International collaboration is much easier when a country can be counted on not to [Flip-Flop](Flip-Flopping)

[1]: https://www.makevotesmatter.org.uk/environment
