# TheMajority

## A website to represent the majority of voters

### Contributing

The majority of the site is built from markdown, to contribute just edit the page(s) and submit a pull request.

A couple of things to note:

* All pages are flattened to TheMajority.uk/pagename
* The wikilinks extension is enabled (e.g `[[link]]` will link to TheMajority.uk/link).

You can test changes locally using `make test-local`

Additionally jinja is supported for pages

#### Jinja

[Jinja](https://jinja.palletsprojects.com) can be used in filenames that end with `md.jinja`

##### Tables

Some functions are included for tables in `functions.jinja`. To use them

1. Add table data to `src/tables/<data>.yaml`
2. Include `{% import 'functions.jinja' as functions %}` at the top of the file
3. Render the data using `{{ functions.format_table('<data>', 'TableName') }}`

#### Python

Python is used to generate the site, the code is procedural and relatively simply

When updating the python code make sure changes lint using `make lint`

### Deployment

Deployment is done via `make deploy` and requires an AWS account (first deployment will require a change to dns providers records)

### TODO

1. Fix style
2. Generate index pages
3. replace npm's `eslint` with a python `javascript linter`
