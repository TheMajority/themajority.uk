# Every Movement needs a soundtrack, here are some relevant songs.

#### To add songs [submit a a pull request](https://gitlab.com/TheMajority/TheMajority.uk/edit/master/paylist.md) here or [contact us](mailto:music@themajority.uk)

* Name - artist - [![Youtube](images/png/Youtube.png)](https://www.youtube.com) - [![Spotify](images/svg/Spotify.svg)](https://open.Spotify.com)
* One of us - Fever 333 - [![Youtube](images/png/Youtube.png)](https://www.youtube.com/watch?v=Ap6Wr-BO6sU) - [![Spotify](images/svg/Spotify.svg)](https://open.Spotify.com/track/51rdCmgHBzg9HJ7eHjVRK9)
