# Fair voting systems lead to stability

## Fair systems provide stable policy

### Under fair systems plans can be made for multiple-election cycles as the next government is unlikely to throw out everything the previous one has done
### Under fair systems voters can affect the makeup of the coalition without having it fall apart

## First Past the Post is not stable

### [Between 1945 and 1998, countries with FPTP had more elections on average than those with PR](https://www.makevotesmatter.org.uk/first-past-the-post)

### [Since WWII the UK has had 21 elections instead of 15](https://en.wikipedia.org/wiki/List_of_United_Kingdom_general_elections) (74 years)

#### 40% more elections than a stable system would produce
#### Only 5 Election results stood for their full 5 years<br><sup>15 were called early either for political gain or due to no confidence votes</sup>
#### Only the 2010 coalition had support from a majority of the population
#### [15 leaders, across 13 different Governments](https://en.wikipedia.org/wiki/List_of_prime_ministers_of_the_United_Kingdom)
##### 7 elections completely replacing the the government

## Proportional systems provide electoral stability<br><sup>While the makeup of the coalition is responsive to vote share</sup>

#### Germany<br><sup>8 elections since reunification (39 years), 0 more than expected</sup>
##### 7 Full elections cycles<br><sup>Just 1 premature election</sup>
##### 3 leaders, across 5 coalitions
##### In all but 1 election, some continuity was kept in government
##### Makeup of coalition has shifted between 5 major parties

#### Norway<br><sup>19 elections since WII (74 years), 0 more than expected</sup>
##### Every election cycle lasted it's full 4 years<sup>0 premature elections</sup>
##### 6 elections completely replacing the the government

#### Finland<br><sup>21 elections since WWII (74 years)<suo>
##### [Just 3 premature elections since WWII](https://en.wikipedia.org/wiki/Parliament_of_Finland#Dissolutions_of_Parliament)
##### [1 time the government been completely replaced](https://en.wikipedia.org/wiki/Fagerholm_I_Cabinet), otherwise at least one party has remained stable between coalitions<br><sup>14 different parties have made up the coalitions at different times</sup>
###### Note there were also 6 caretaker governments in this time, but without a mandate to [lip-flop](Flip-Flopping) policies</sup>
