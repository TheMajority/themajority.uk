# Education benefits significantly from more stable Government

#### How fair voting systems provide [stable](Stable) government is outlined [here](Stable)

##### Constant [[Flip-Flopping]] in organisational structures don't provide a stable environment for teachers or student

#### The [[Flip-Flopping]] on the syllabus also prevents refinement and improvements of education & materials

## Fair voting would provide the stability for teachers to refine rather than replace materials & methodologies

### This would allow the UK and/or the US to get back to leading the world in terms of quality Education
