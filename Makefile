.PHONY: help all local install clean lint build test deploy clear-cache deploy-infra test-site test-extra
SITENAME = themajority.uk
SHELL = /bin/bash
PORT := $(shell shuf -i 1025-65000 -n 1)

help:
	@echo  'Targets:'
	@echo  '  * all [deploy clear-cache test-site]'
	@echo  '  * local [clean install lint build test]'
	@echo  '  * deploy [local deploy-infra sync]'
	@echo  '  * test-extra - check with non managed dependencies (checkmake shellcheck)'

all: deploy clear-cache test-site
local: clean install lint build test

clean:
	rm -rf dist/* node_modules/ file:/ package-lock.json

install:
	pip install -r requirements.txt	
	npm install

lint:
	black scripts/
	flake8 scripts/ || true
	pylint scripts/*.py || true
	npm run lint
	yamllint .
	cfn-lint cloudformation/*

build:
	./scripts/generate.py

test:
	./scripts/spellcheck.sh
	out=$$(grep "[\[\]{}]" dist/[A-Z]*) ; if [[ "$$out" ]] ; then echo "$$out" ; exit 1 ; fi
	./scripts/serve.py $(PORT) &> /dev/null &
	linkchecker --check-extern http://127.0.0.1:$(PORT)/{index,404}
	echo "$$(ps a| grep "[s]erve.py $(PORT)" | cut -f 1 -d\  )"
	pkill -f serve.py

test-extra:
	shellcheck scripts/*.sh
	checkmake Makefile

deploy: local deploy-infra sync

deploy-infra:
	./scripts/deploy.sh $(SITENAME)

sync:
	./scripts/sync.sh $(SITENAME)

clear-cache:
	./scripts/clear-cache.sh $(SITENAME)

test-site:
	linkchecker --check-extern http{s,}://$(SITENAME){,/404}
