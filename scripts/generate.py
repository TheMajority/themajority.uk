#!/usr/bin/env python3
import sys
import os
import yaml
import markdown
import jinja2
from markdown.extensions.wikilinks import WikiLinkExtension


def get_data():
    data = {}
    for filename in ["solutions", "recommendations", "faq"]:
        path = os.path.join("src", "tables", f"{filename}.yaml")
        data[filename] = yaml.safe_load(open(path))

    return data


def get_all_headers(data):
    headers = {}
    for source, tables in data.items():
        headers[source] = {}
        for name, table in tables.items():
            if isinstance(table, list):
                headers[source][name] = get_headers(table)
            elif isinstance(table, dict):
                if name in headers:
                    print(f"conflict generating headers {source} {name}")
                    sys.exit(1)

                headers[name] = get_all_headers({name: table})

    return headers


def get_headers(table_data):
    all_headers = [list(row.keys()) for row in table_data]
    headers = max(all_headers, key=len)

    for row_headers in all_headers:
        if not all([key in headers for key in row_headers]):
            print("A row in must contain all the headers to determine correct ordering")
            print(f"longest row in {headers}")
            print(f"extra headers in {row_headers}")
            sys.exit(1)

    return headers


def get_pages():
    pages = []
    for root, _dirs, files in os.walk("src"):
        for name in files:
            if name[1] != "." and ".md" in name:
                filename = os.path.join(root, name)[4:]
                filename = filename.split(".")[0]
                pages.append(filename)

    return pages


def generate_site():
    data = get_data()
    headers = get_all_headers(data)
    pages = get_pages()
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader("src/"), lstrip_blocks=True, trim_blocks=True
    )
    env.globals = {"data": data, "headers": headers, "pages": pages}

    indexes = {}
    for root, _dirs, files in os.walk("src"):
        if root not in env.loader.searchpath:
            env.loader.searchpath.append(root)
        for name in files:
            filepath = os.path.join(root, name)
            if name.startswith("."):
                continue
            if ".md" in filepath:
                generate_file(filepath, name, env)
                if os.path.dirname(filepath) != "src":
                    flatten_path(filepath, indexes)
            elif filepath.split(".")[-1] in ["css", "js"]:
                make_link(filepath, "src")

    for root, _dirs, files in os.walk("resources"):
        for name in files:
            filepath = os.path.join(root, name)
            make_link(filepath, "resources")

    # generate_indexes(indexes, env)


def generate_file(filepath, name, env):
    print(filepath, end=",")
    # try:
    if name.endswith(".jinja"):
        text = env.get_template(name).render()
    else:
        text = open(filepath).read()

    generate_markdown(filepath, text, env)
    # except Exception as err:
    # print("error,", err)


def generate_markdown(source, text, env):
    content = markdown.markdown(
        text, extensions=["extra", "toc", WikiLinkExtension(end_url="")]
    )
    html = env.get_template("page-template.html.jinja").render(
        content=content, filename=source
    )

    write_html(source, html)
    print("generated")


def write_html(source, html):
    filename = basenameonly(source)
    dest = os.path.join("dist", filename)
    with open(dest, "w") as output:
        output.write(html)


def generate_indexes(indexes, env):
    for page, links in indexes.items():
        env.globals["sub-pages"] = links
        env.globals["name"] = page.split(os.path.sep)[-1]
        return
        # text = env.get_template("index.template.md.jinja").render()
        # generate_markdown(page, text)


def make_link(src, prefix):
    dest = src.replace(prefix, "dist", 1)
    dest_dir = os.path.dirname(dest)
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    if not os.path.exists(dest):
        os.link(src, dest, follow_symlinks=True)
    elif not os.path.samefile(src, dest):
        os.remove(dest)
        os.link(src, dest, follow_symlinks=True)

    print(src, "->", dest)


def flatten_path(filepath, indexes):
    destpath = filepath[4:]
    destdir = os.path.dirname(destpath)
    filename = basenameonly(filepath)

    if destdir not in indexes:
        indexes[destdir] = []

    indexes[destdir].append(filename)


def basenameonly(filename):
    return os.path.basename(filename).split(".")[0]


if __name__ == "__main__":
    generate_site()
