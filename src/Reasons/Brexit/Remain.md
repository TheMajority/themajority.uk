# Remain voters should support fair electoral systems

## Brexit wouldn't have happened without FPTP

### Parties backing a public vote on the deal, won more votes in both [May][1] & [December][2] 2019

### Popular support was [has been for remain since 2017][3]
#### However the 2.5 party system has been too rigid to allow this to be heard
##### Splits and [mistakes in tactical voting][4] have given remain seats to leaver MPs

### Re-Entry into the EU, will require [stability](Stable)

### A significant part of the Brexit vote was a protest vote, by [[Ignored]] Voters<br><sup>Fair voting systems would have prevented them being ignored in the first place</sup>

[1]: https://en.wikipedia.org/wiki/2019_European_Parliament_election_in_the_United_Kingdom
[2]: https://en.wikipedia.org/wiki/2019_United_Kingdom_general_election
[3]: https://en.wikipedia.org/wiki/Opinion_polling_on_the_United_Kingdom%27s_membership_of_the_European_Union_(2016%E2%80%93present)#Remain/leave
[4]: https://en.wikipedia.org/wiki/Kensington_(UK_Parliament_constituency)#Elections_in_the_2010s
