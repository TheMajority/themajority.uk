# Smaller parties would benefit significantly from a fair voting system

## *"The ability of new parties to emerge is essential for a functioning democracy"*

### Getting the correct number of representatives is a pretty significant benefit

#### Once votes for a party are not seen as wasted, voters can shift away from the large stagnant parties to parties they actually support

### Once voting becomes about what you want, rather than what you hate the least, smaller parties are essential in shaping the future

### Existing representatives are also able to re-align without having to help prop up parties they don't fully agree without

## Because of this most smaller parties already support proportional representation
