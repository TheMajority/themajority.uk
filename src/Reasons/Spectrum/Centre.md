# The Centre is better served by fair voting systems

## First past the post pushes voters to the extremes

### under FPTP, [broad church parties](Large) are preferred, these inevitably end up radicalised by more active party members who often lose track of the centre<br><sup>as more active party members have a more significant impact on the party's direction than voters</sup>
### Fair voting systems protects against parties moving to extremes by allow more radical elements to split without the current risks of splitting the vote<br><sup>e.g PSOE->Podemos</sup>
### If all parties moving away from the centre, it is also much easier for new parties to emerge.
### [Coalitions](Stable) also favour centrist parties as they can go into stable coalition both with a wider range of potential partners

<center>

#### centre-left + left, centre, centre-right, single issue parties
#### centre + centre-left centre-right, single issue parties
#### centre-right + right, centre, centre-left, single issue parties

</center>
