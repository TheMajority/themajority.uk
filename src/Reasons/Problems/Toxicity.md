# We currently have some of the most toxic and divisive politics since WWII

## The voting system is part of the problem

#### Changing to a better system provides the following benefits

### <u>All alternatives</u><br><sup>Increasing the number of viable parties, which</sup>
#### Makes negative campaigning less effective
##### preventing votes for other parties no longer has the same benefits as **gaining votes**
#### Makes it more likely that parties will share policies<br><sup>so attacking a party/party leader to prevent a policy is less effective</sup>

### <u>Proportional systems</u><br><sup>make coalitions are more likely, which</sup>
#### Makes negative campaigning less advisable as you may end up working with your competitors
#### Means [coalition](Stable) parties have to put forward positive policies to differentiate themselves

### <u>Preferential systems</u>
#### Negative campaigns are counter-productive as candidates still want to rank well with their opponents voters 
