#!/usr/bin/env python
import sys
import os
from http import server


def set_dir():
    directory = os.path.dirname(__file__)
    directory = os.path.join(directory, "..", "dist")
    os.chdir(directory)


def get_httpd():
    port = int(sys.argv[1]) if len(sys.argv) > 1 else 8000
    handler = server.SimpleHTTPRequestHandler
    handler.extensions_map = {
        ".png": "image/png",
        ".jpg": "image/jpg",
        ".svg": "image/svg+xml",
        ".css": "text/css",
        ".js": "application/x-javascript",
        "": "text/html",
    }

    httpd = server.HTTPServer(("127.0.0.1", port), handler)
    return httpd


if __name__ == "__main__":
    set_dir()
    HTTPD = get_httpd()
    HTTPD.serve_forever()
