# The Economy suffers under FPTP

### FPTP results in

#### Large shifts in policy make it difficult to do effective long term planning
##### This is especially tough for small businesses
#### [[Flip-Flopping]] on policy makes government investment in the economy unreliable<br><sup>and less effective as a result</sup>

### [Stable coalitions](Stable) can provide

#### A better economy to allow businesses to grow
#### The stability to make government investment more effective
