# FPTP breads corruption in at least 3 ways

### FPTP lets parties survive corruption scandals

#### With fewer parties Voters Are unlikely to cross to "the other party" over "just one scandal"

#### Party Politicians & Members cannot leave to form a new party

### FPTP Enables corruption as only the party in government has the ability to trigger a serious inquiry into scandals

#### Parties will often cover-up or whitewash themselves

##### This is far less likely under a coalition, where partners have more to gain by exposing corruption

### Electoral manipulation is much easier, as fewer constituencies have significant influence in elections

#### Including direct vote manipulation

##### Electoral manipulation also includes buying votes, by promising national funds to swing seats
