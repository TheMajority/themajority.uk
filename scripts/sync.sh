#!/usr/bin/env bash
SITENAME=$1
DIST_DIR="$( dirname "${BASH_SOURCE[0]}" )/../dist"

cd "$DIST_DIR" || exit 1
echo "Sycning real files"
aws s3 sync --exclude ".*" --exclude "*.*" --content-type text/html . "s3://${SITENAME}/"

emptyfile=$(mktemp)
echo "Sycning lowercase redirects"
for file in * ; do
  [ -f "$file" ] && [ "$file" != "${file,,}" ] && [ ! -f "${file,,}" ] && aws s3 cp "$emptyfile" "s3://${SITENAME}/${file,,}" --website-redirect "/$file" && touch "${file,,}"
done

echo "Sycning manual redirects"
while IFS=, read -r src dest ; do
    [ ! -f "$src" ] && aws s3 cp "$emptyfile" "s3://${SITENAME}/$src" --website-redirect "/$dest" && touch "$src"
    [ "$src" != "${src,,}" ] && [ ! -f "${src,,}" ] && aws s3 cp "$emptyfile" "s3://${SITENAME}/${src,,}" --website-redirect "/$dest" && touch "${src,,}"
done < ../redirects.csv

echo "Sycning deletions"
aws s3 sync --exclude ".*" --delete . "s3://${SITENAME}/"

rm "$emptyfile"
