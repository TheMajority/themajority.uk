// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

const styleSets = {
  text: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'sup' ],
  head: ['h1', 'h2'],
  s1: ['h3', 'h5' ],
  s2: ['h4', 'h6' ]
}

function updateColors () { // eslint-disable-line no-unused-vars
  var params = getParams(location.search)
  for (const key in params) {
    var value = params[key]
    if (key in styleSets) {
      styleSets[key].forEach(function (element) {
        updateElementsByTag(element, updateColor.bind(this, value))
      })
    } else if (key === 'bg') {
      document.getElementsByTagName('body')[0].style.backgroundColor = value
    } else {
      updateElementsByTag(key, updateColor.bind(this, value))
    }
  }

  updateLinks()
}

function getParams (searchString) {
  var params = {}
  searchString.substring(1).split('&').forEach(
    function (paramString) {
      var paramArray = paramString.split('=')
      if (paramArray.length > 1) {
        params[paramArray[0]] = paramArray[1]
      }
    }
  )
  return params
}

function updateElementsByTag (tag, f) {
  var elements = document.getElementsByTagName(tag)
  for (var i = 0; i < elements.length; i++) {
    f(elements[i])
  }
}

function updateColor (color, element) {
  console.log(color, element)
  element.style.color = color
}

function updateLinks () {
  updateElementsByTag('a', unifyLinkParams)
}

function unifyLinkParams (element) {
  if (element.href.startsWith(location.origin)) {
    element.search = location.search
  }
}

function share () { // eslint-disable-line no-unused-vars
  var shareData = {
    title: 'The Majority: ' + location.pathname.slice(1),
    text: document.title,
    url: document.URL
  }
  try {
    navigator.share()
  } catch (err) {
    shareData = [shareData.title, shareData.text, shareData.Url, ''].join('\n')
    navigator.clipboard.writeText(shareData).then(function () {
    }, function () {
      window.alert("Browser doesn't support Share API, message would have been\n\n" + shareData)
    })
  }
}
