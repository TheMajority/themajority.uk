# Conservatives voters would benefit from the end of FPTP

#### See also [reasons large parties should support proportional representation](Large)

###### While the Conservatives are less fractured than [[Labour]], there are still big divisions

<br>||<br>
---:||:---
Cameron |vs| Boris
Pro-business |vs| Anti-business
Remain |vs| Leave
Urban |vs| Rural

#### <div align="left">A shift in party machinery has left centrist voters supporting an anti-business leader</div>
#### <div align="right">Anti-establishment Brexit voters have been left supporting yet another Oxbridge Etonian</div>
##### Allowing new parties to emerge can either allow voters to move to a centre party or allow further right voters to move to a less centrist party
#### Either way people can start voting for leader they want, rather than whoever can keep Labour out.
