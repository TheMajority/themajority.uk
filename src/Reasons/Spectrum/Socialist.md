# Socialism

## *"Socialism without democracy is pseudo-socialism, just as democracy without socialism is pseudo-democracy"*

#### See also [[Left]], [[Labour]] & [[Democrats]]

## Democracy

#### In the 21st century there is no case for non-democratic socialism, not only is it morally wrong given the ability to inform & educate people,
#### a "dictatorship of the proletariat", is not only morally wrong but it has never worked over the long term.
#### Suppressing opposing views just makes counter revolutionary forces stronger, weather this suppression is done with violence or via electoral systems
#### So for socialism to succeed it **must** be done with the support of the people
#### Therefore electoral reform needs equal billing with other socialist ideals

### Working Government

#### Faith in the state to run services is key to socialism, and proportional representation provides stable coalition governments, that are still responsive and accountable to the electorate
#### Oversight of the state bureaucracy and state forces (e.g police, military, etc) is also more effective under moderate improvement rather than flip-flopping policy changes.


### [why socialism?](https://monthlyreview.org/2009/05/01/why-socialism/)
#### While Electoral reform alone doesn't answer the question Einstein posed, it certainly has to be a significant part of it

> The achievement of socialism requires the solution of some extremely difficult socio-political problems: how is it possible, in view of the far-reaching centralization of political and economic power, to prevent bureaucracy from becoming all-powerful and overweening? How can the rights of the individual be protected and therewith a democratic counterweight to the power of bureaucracy be assured?

#### Other key parts being
##### Greater Transparency<br><sup>now possible due to greater accessibility to information</sup>
##### Greater Decentralisation<br><sup>now possible due to instant transfer of information</sup>

### Working Examples
If you look at left leaning countries (often called Democratic socialists), they all have Proportional systems:

* Europe
    * Nordics
        * Denmark
        * Finland
        * Sweden
        * Norway (1993-2013)
        * Greenland
        * Iceland
    * Non-Nordic:
        * Portugal
        * San Marino (PR with top-up seats)
* Latin America
    * Uruguay
    * Nicaragua
    * Bolivia (2006 – 2019)
    * Ecuador (2007 – 2017)

And even in countries whose governments are not left leaning, the democratic accountability and stability delivered by PR, has ensured a strong state and strong workers rights, such as:

* Germany
* Netherlands
* Belgium

Obviously changing the voting system in of itself isn't going to move the country to the left/right, however every example of a functioning left wing government comes from a country using PR

