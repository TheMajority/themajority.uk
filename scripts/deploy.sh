#!/usr/bin/env bash
set -e
SITENAME=$1
CFN_DIR="$( dirname "${BASH_SOURCE[0]}" )/../cloudformation"
STACKNAME=${SITENAME//./}
DEPLOY_CMD="aws cloudformation deploy --no-fail-on-empty-changeset --tags Classification=Public Site=$SITENAME"

deploy(){
    # shellcheck disable=SC2145
    echo "deploying $1 ($2) [${@:3}]"
    # shellcheck disable=SC2068
    $DEPLOY_CMD --stack-name "$1" --template-file "${CFN_DIR}/$2" --parameter-overrides ${@:3}
    aws cloudformation wait stack-exists --stack-name "$1"
}

AWS_DEFAULT_REGION=us-east-1 deploy "${STACKNAME}-certificate" certificate.yaml SiteName="$SITENAME"

CERT=$(aws cloudformation describe-stacks --region us-east-1 --stack-name themajorityuk-certificate --query Stacks[0].Outputs[0].OutputValue --output text)
deploy "$STACKNAME" site.yaml SiteName="$SITENAME" CertificateARN="$CERT"

if [ -n "$2" ] ; then
   # shellcheck disable=SC2016
   HOSTEDZONE=$(aws cloudformation describe-stacks --stack-name "$STACKNAME" --query 'Stacks[0].Outputs[?OutputKey==`HostedZoneId`].OutputValue' --output text)
   deploy "${STACKNAME}-mail" mail.yaml SiteName="$SITENAME" Route53Zone="$HOSTEDZONE" MailServer="$2"

fi


