# Healthcare

## *"Fair Voting systems will make **you** live longer"*

### <u>Problems caused by FPTP</u>

#### Constant shifts in direction don't provide an effective healthcare system

#### Short term cost savings and investments, end up costing us more in the longer term
##### Shutting down A&E, balances the budget, but the cost of treating patients after a 45 minute ambulance ride is significantly higher and adds up over time
##### Similarly opening new hospitals wins votes, but without proper planning, they will pull staff and resources from existing services, resulting in worse overall care of patients
#### [Training](Education) medical staff takes long term planning
#### [[Flip-Flopping]] makes attracting medical staff from abroad difficult, as medical staff need
##### Stable the plans and schemes we offer
##### A stable environment & [[Economy]] to migrate to

### <u>Fair elections would</u>

#### Deliver the [stability](Stable) to train/attract medical staff
#### Enable long term efficient running of medical services
