# Flip-flop politics is terrible for the country

## *"Under FPTP it's common for one party to undo the work of the previous regardless of impact"*

### Constant flip-flopping isn't an efficient way to run anything

##### Foreign relations, are difficult if there is no certainty that you will keep promises for 5 years
##### Hiring the best, is difficult to if they are replaced every election cycles

##### Large scale projects need to be rushed to fit into election cycles
###### Rushing projects results in overspending
###### It can even lead to [corruption](Corruption) to make it happen in time or to cover up overspending

##### Many sectors need planning for longer than election cycles to be effective
###### [[Economy]]
###### [[Environment]]
###### [[Education]]
###### [[Healthcare]]
