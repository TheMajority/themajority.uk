# LGBTQI+ rights are handled better under fair voting systems

#### LGBTQI+ rights face similar problems to a lot [single issues](Single)
##### Especially as that under [toxic bi-partisan system](Toxicity) there is often a knee-jerk pushback

### [[Flip-Flopping]] politics can lead to the rollback rights<br><sup>often simply to undo the work of the opposition</sup>
#### rollbacks leave LGBTQI+ individuals to more uncertainty while those rights are in lace
#### rollbacks make life more dangerous for LGBTQI+ individuals who chose to be more open about their identity/relationship<br><sup>especially when their rights are removed</sup>

### <u>Proportional representation provides</u>

#### Stability for LGBTQI+ rights
#### The ability for [smaller dedicated parties](Small) to form around LGBTQI+ rights<br><sup>rather than waiting for [larger parties](Large) to prioritise them</sup>

