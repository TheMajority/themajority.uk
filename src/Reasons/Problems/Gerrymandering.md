# Proportional systems are safe from Gerrymandering

## [Gerrymandering is a practice of establishing unfair political advantage for a particular party or group by manipulating district boundaries](https://en.wikipedia.org/wiki/Gerrymandering)

### Proportional systems are immune to boundary manipulation  up to the scale to which they are proportional<br><sup>This scale is usually large historically areas, such as entire countries, states or counties</sup>

### While boundaries can still have an impact under preferential/single winner system, the ability for new parties to emerge makes it far less effective as a tool for manipulation<sup>compared to under FPTP</sup>

