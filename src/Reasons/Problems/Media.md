# The media is becoming increasingly polarised

## [Trust in the media is being eroded](https://www.ipsos.com/ipsos-mori/en-uk/impact-declining-trust-media)

### Partisan feedback loop

#### People pick media they agree with, this causes the media to chase voters, which then further re-enforces the media they pick.

## This effect is particularly pronounces when there are just 2 parties capable of winning elections.

[a more detailed analysis is available here](https://www.oxfordscholarship.com/mobile/view/10.1093/oso/9780190923624.001.0001/oso-9780190923624-chapter-3)

### Improvements

##### [Reducing the toxicity in politics](Toxicity) will help dampen the feedback loop

#### Increasing the number of viable parties, will also push papers to be more neutral to avoid alienating reader of n-1 parties
