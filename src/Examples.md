# Examples

## There is no point in talking about abstract systems when we have real world data

Here is a comparison of some real world implementations of MMP, STV & FPTP

* See the [[Solutions]] pages for a description of the relevant systems
* Only parties which would have got 1 or more seats under fair allocation are listed
* Totals for votes are just for parties that got seats

### Summary

System|Country|Worst votes/seat|Seat Diff|% Seat Diff
:---|:---:|:---:|:---:|:---:
MMP|Germany|1.005|0|0
MMP|New Zealand|1.01|4|3.3%
MMP|Scotland|1.48|13|10%
STV|Ireland (2020)|3.06|8|5.03%
STV|Ireland (2016)|2.19|26|17%
FPTP|United Kingdom|17.58|208|32%

### MMP

#### ![flag](images/flags/DE.svg){: .flag } Germany ![flag](images/flags/DE.svg){: .flag }

Party|Party Votes|Seats|Votes/Seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Alternative for Germany|5878115|94|62,533|1.005|0
Free Democratic Party|4999449|80|62,493|1.004|0
Christian Social Union in Bavaria|2869688|46|62,385|1.002|0
Social Democratic Party|9539381|153|62,349|1.002|0
The Left|4297270|69|62,279|1.001|0
Christian Democratic Union|12447656|200|62,238|0.999|0
Alliance 90/The Greens|4158400|67|62,066|0.996|0
Total|44189959|709|62,327||0
||||% seat diff|0.0%

Germany uses large regional (approx 14) additional allocations combined with overhang seats to ensure fair results.

The overhang seats, combined with not transferring votes from independent winners, prevents decoy lists.

Type|Count|%
---|---:|---
Geographic|299|42%
Additional allocation|299|42%
Overhang levelling|111|16%
Total|709|


#### ![flag](images/flags/NZ.svg){: .flag } New Zealand ![flag](images/flags/NZ.svg){: .flag }

Party|Party Votes|Seats|Votes/Seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Labour|956184|46|20787|1.01|0
NZ First|186706|9|20745|1.01|0
National|1152075|56|20573|1.00|0
Green|162443|8|20305|0.99|0
ACT|13075|1|13075|0.64|0
Opportunities|63260|0|inf|inf|-3
Māori |30580|0|inf|inf|-1
Total*|2470483|120|20587||4
||||% seat diff|3.3%

New Zealand uses a country wide additional allocation system

Type|Count|%
---|---:|---
Geographic|71|59%
Additional allocation|49|41%
Total|120|

#### ![flag](images/flags/GB-SCO.svg){: .flag } Scotland ![flag](images/flags/GB-SCO.svg){: .flag }

Party|Party Votes|Seats|Votes/Seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Scottish Green|150426|6|25,071|1.48|-2
Liberal Democrats|119284|5|23,857|1.41|-2
Labour|435919|24|18,163|1.07|-1
Conservative|524222|31|16,910|1.00|0
SNP|953587|63|15,136|0.89|6
UKIP|46426|0|inf|inf|-2
Total*|2183438|129|16,926||13
||||% seat diff|10%

Scotland uses small regions (7 additional member each)
So despite having more additional members than New Zealand achieves less fair results

Type|Count|%
---|---:|---
Geographic|73|57%
Additional allocation|56|43%
Total|129|

### STV (![flag](images/flags/IE.svg){: .flag } Ireland ![flag](images/flags/IE.svg){: .flag })

Ireland uses constituencies with 3-5 seats

#### 2020

Party|First Preference Votes|Seats|votes/seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Aontú|41614|1|41614|3.06|-2
Labour Party|95588|6|15931|1.17|-1
Sinn Féin|535595|37|14476|1.06|-2
Independent|266529|19|14028|1.03|0
Fianna Fáil|484320|37|13090|0.96|1
Fine Gael|455584|35|13017|0.96|1
Green Party|155700|12|12975|0.95|0
Solidarity–PBP|57420|5|11484|0.84|0
Social Democrats|63404|6|10567|0.78|1
Independents 4 Change|8421|1|8421|0.62|0
total|2164175|159|13611||8
||||% seat diff|5.03%

#### 2016

Party|First Preference Votes|Seats|votes/seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Green Party|57,999|2|29,000|2.19|-2
Social Democrats|64,094|3|21,365|1.62|-1
Labour Party|140,898|7|20,128|1.52|-3
Independent|249,285|13|19,176|1.45|-5
Independent Alliance|88,930|6|14,822|1.12|0
AAA–PBP|84,168|6|14,028|1.06|0
Sinn Féin|295,319|23|12,840|0.97|0
Fianna Fáil|519,356|44|11,804|0.89|4
Fine Gael|544,140|49|11,105|0.84|7
Independents 4 Change|31,365|4|7,841|0.59|1
Renua|46,552|0|inf|inf|-3
Total*|2,075,554|157|13,220|-|26
||||% seat diff|17%


#### FPTP (![flag](images/flags/UK.svg){: .flag } United Kingdom ![flag](images/flags/UK.svg){: .flag })

Party|Votes|Seats|Votes/Seat|Ratio|Seat Diff
---:|---|:---|---:|:---|:---:
Green Party of England and Wales|835579|1|835,579|17.58|-16
Liberal Democrats|3696423|11|336,038|7.07|-66
Alliance Party of Northern Ireland|134115|1|134,115|2.82|-1
Social Democratic and Labour Party|118737|2|59,369|1.25|0
Labour Party|10269076|202|50,837|1.07|-14
Plaid Cymru|153265|4|38,316|0.81|0
Conservative Party|13966565|365|38,265|0.81|71
Democratic Unionist Party|244128|8|30,516|0.64|2
Sinn Féin|181853|7|25,979|0.55|3
Scottish National Party|1242380|48|25,883|0.54|21
Brexit Party|644257|0|inf|inf|-13
UUP|93123|0|inf|inf|-1
Total|30842121|649|47,523||208
||||% seat diff|32%
