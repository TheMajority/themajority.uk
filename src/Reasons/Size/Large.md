# Large Parties suffer under FPTP

####  Broad Church - Big tent - catch-all

## *"Large parties under FPTP are effectively in an abusive relationship with both their representatives and their voters, knowing they don't have to listen to them as they won't go anywhere"*

### Problems

#### Voters and representatives taken for granted, because they are unlikely to jump ship and similar parties can't emerge

#### When the party gains/loses votes it guesses what voters want
##### <div align="right"><small>Under alternatives, changes are visible, removing the need for a crystal balls to read changes in the "will of the people"</small></div>
#### Infighting for the direction of the party makes the party seem incompetent
##### <div align="right"><small>Being 2 <sup>or more</sup> parties wearing a trenchcoat doesn't work well</small></div>

#### Votes will be taken as support for policies that voters disagree with

### Advantages

##### Moving to proportional or preferential systems would help larger parties

#### Allowing representatives to leave<br><sup><sup>without the votes being wasted</sup></sup>
##### Increases party unity
##### Gives the party Allies<br><sup><sup>Instead of enemies</sup></sup>

##### Prevents <strike>hijacking</strike> sudden shifts of the party
###### Radical movement of the party often alienates much of the existing support base
###### Even if the move is ultimately good achieved gradually with more radical elements done outside of the party, by allies

### Examples

#### While the CDU faces pressure from AfD in Germany it has stayed strong under a proportional system

#### Meanwhile under FPTP

##### Labour has seen sudden shifts due to militant/momentum
##### The pro-business conservative party has been seen over 30K anti-business UKIP members join
##### The GOP has seen a sudden shift due to Tea Party
##### The Democratic suffers from open infighting between it's established representatives and newer more radical members
