# Right wing voters are better served by fair voting systems

### Centre right parties don't need to worry about parties becoming radicalised toward the far right

### All right wing voters can have their voices heard<br><sup>Proportional representation results in less votes being wasted due to vote splitting</sup>

### Most right wing voters value [stability](Stable) & the [[Economy]] <br><sup>both do better under fair voting systems</sup>
