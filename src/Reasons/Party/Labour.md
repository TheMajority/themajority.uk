# Labour struggles due to FPTP

## *"Labour are the only socialist party in the developed world that supports FPTP"*

#### See also reasons [Broad church parties should support proportional representation](Large) & [The Make votes matter report][1]

#### If you are already a convinced Labour voter skip to [next steps](#next-steps)

### Labour specific reasons

#### Voters being taken for granted, has been a major factor in Labour's decline since 97
##### Labour has to guess what voters want when they leave, leading to [factionism](#factions)
#### Even if you do vote Labour, your vote may be used for policies you disagree with
##### <right><small>Labour has never fully recovered from the spectre of the the Iraq war</small></right>
#### Infighting for the direction of the party done on speculation, rather than vote share
#### Drastic changes in direction have lead to splits between Leaders vs MPs vs Members vs Voters

### Factions

###### There is no point in pretending there isn't significant division within the party

||||
---:||:---
Left |vs| Centre-left
Democratic socialist |vs| Social democrats
Corbyn |vs| Blair
Remain |vs| Leave
Urban |vs| Rural

###### While all of the above have significant common ground
##### The infighting over who control the party infrastructure, is effort that should be spent winning votes instead
#### Allowing public coalitions can be made on the basis of vote share rather than speculation


### Benefits<br><sup>of Proportional Representation</sup>

#### [[Left]] [[Centre]] [[Socialist]] party members will Benefit
#### Will strengthen party unity<br><sup>Making winning more likely</sup>

### Next steps

#### See [Make votes matter](https://www.makevotesmatter.org.uk/labour4pr) for next steps to further Labour support for PR
#### Join [LCER (Labour Campaign for Electoral Reform)](https://www.labourcampaignforelectoralreform.org.uk/join-or-donate)

#### Vote in the Leadership contest

##### Leader: Starmer, Nandy/Long-Bailey
##### Deputy Leader: Murray, Allin-Khan, Burgon/Rayner

##### Leader views

Candidate|Position|Statement
---:|---|:---
Starmer, Keir|Undecided (positive)|I don't have a fixed view on this.<br>There are clearly lots of things wrong with the system as it currently is<br>and it is worthwhile having the discussion about alternative models.
Nandy, Lisa|Undecided|I haven’t moved. I think there’s pros and cons basically.
Long-Bailey, Rebecca|Undecided|

##### Deputy Leader views

Candidate|Position|Statement
---:|---|:---
Murray, Ian|Supports PR|I'm a supporter of a PR system and, indeed, supported a Private Members Bill from my friend and colleague, Jonathon Reynolds, just last year.
Allin-Khan, Rosena|Undecided (positive)|While the 2011 referendum on the introduction of the Alternative Vote was defeated by a margin of more than two to one,<br>I do not think electoral reform should be off the agenda if there is public support for it.<br><br>I therefore welcome that the Leader of the Opposition has announced that electoral reform will be considered as part of the Opposition's Constitutional Convention.<br>More widely, I believe it is vital that our politics connects and engages with the public and I am open to proposals that can help achieve this.
Burgon, Richard|Undecided (slightly positive)|I'm more open minded to it Proportional Representation than I used to be.
Rayner, Angela|Undecided (slightly positive)|I acknowledge however that forms of proportional representation are already being used successfully in the devolved administrations across the UK, as well as in many local authority elections.
Butler, Dawn|Undecided (wrongly)|There are, of course, strengths and weaknesses to all voting systems.<br> FPTP does have a history of generally returning stable<sup>wrong see [[Stable]]</sup>, single-party governments and of retaining the constituency link<sup>as does MMP</sup>, both of which I think are important benefits to our electoral system.<br><br>I appreciate, however, that there is a case to look in detail at our electoral system and that forms of proportional representation are already used in the devolved administrations across the UK, as well as in many local authority elections.

## Only by enabling the emergence of new parties, what is currently he Labour party can both prioritise winning AND concentrate on implementing radical change

[1]: https://static1.squarespace.com/static/563e2841e4b09a6ae020bd67/t/59c54845cd39c36b4fe46fe2/1506101358963/TMNTF+-+online+version.pdf
