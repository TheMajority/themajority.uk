# Leave voters should support fair electoral systems

### Leave voters couldn't have been [[Ignored]] for so long, as UKIP would have had a fair allocation of MPs<sup>since 2004</sup>

### Making a success of Brexit requires [stability](Stable)

#### Stability is needed to underpin **international relations** in order to get good trade deals
#### [Economic](Economy) [stability](Stable) is essential in order for businesses to grow
