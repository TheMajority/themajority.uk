# The left is better represented under fair voting systems

## *"FPTP electoral system has a pronounced conservative bias"*

#### See also [[Socialist]], [[Labour]] & [[Democrats]]

### Usually the left is more fragmented, as left-wing groups have very different priorities, resulting more factions/parties<br><sup>Despite agreement on many issues</sup>
#### Proportional representation results in less left wing votes being wasted due to vote splitting

### Trust in the government to run services is required for a lot of left wing goals
#### [Stable coalitions](Stable) are more capable of running services effectively
##### They can build on the previous terms work adjusting to fit the public's changing requirements
##### Coalitions also benefit from inter-party competition

### Stretched police/army often get less oversight in order to remain **"effective"**<br><sup>this runs contrary to much left wing thinking</sup>
#### [Stable coalitions](Stable) provide more gradual change for police/army
#### [Stable coalitions](Stable) allow successive terms to build effective oversight
