# Voters that primarily care about a **single** issue are better represented by fair voting systems


### Under FPTP single issue voters or people who care primary about one thing, face a dilemma

#### Vote for a [small party](Small) that can't win to apply pressure to other parties<br><sup>hurting allied parties</sup>

#### Vote for a [large party](Large) they disagree with on most other issues<br><sup>hurting allied parties, who disagree on this issue</sup>

#### Vote for a [large party](Large) they mostly agree with, hope they prioritise your issue<br><sup>they can join the party and exert internal pressure<br>still not as effective as explicitly vote for what you want</sup>

### Additionally due to [partisan politics](Toxicity) when one side supports them, the other may oppose as a knee-jerk reaction

### Single issue's include
#### [The environment](Environment)
#### [LGBTQI+ issues](LGBTQI+)
#### [[Leave]] **and** [[Remain]]
#### Gun rights **and** Gun control
#### Pro-Life **and** Pro-Choice
##### And many others

