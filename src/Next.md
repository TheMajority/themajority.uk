# Vision without action is merely a dream

## *"The 2010s were a bad decade for democracy, it's time for democracy to **strike back**"*

### What effective next steps can be taken

###### Note this call to action is based on a uk-centric point of view, while I think it should apply everywhere take it with a pinch of salt.

##### Skip to [Party Members](#party-members) - [Bands](#bands) - [Graphics Designers](#graphic-designers)

#### Everybody

##### Spread the word, while we are the majority not everybody is aware of that
##### Refuse to call what leaders has a majority, it's a plurality at best<br><sup><sup>It sounds petty but words matter, it's important to remind people</sup></sup>
##### Remind people that the majority oppose the government
##### Join a party you <sup>mostly</sup> agree with<br><sup><sup>You can't effect change outside of them</sup></sup>
##### [Update](https://gitlab.com/TheMajority/TheMajority.uk/) content on this site

##### Join a relevant organisation

<br>|<br>
---:|---
<br>|[![ACE Project](images/jpg/ACE_Project_Logo.jpg)](https://aceproject.org/) [![Reddit](images/svg/Reddit.svg)](https://old.reddit.com/user/_The_Majority_/m/electoralreform/)
[![UK](images/flags/UK.svg)](UK) | [![Make Votes Matter](images/png/Make_Votes_Matter_logo.png)](https://www.makevotesmatter.org.uk/) [![Electoral Reform Society](images/svg/Electoral-reform-society.svg)](https://www.electoral-reform.org.uk/)
[![USA](images/flags/US.svg)](USA) | [![FairVote](images/jpg/FairVote_Logo.jpg)](https://www.fairvote.org/) [![The_Center_for_Election_Science](images/png/The_Center_for_Election_Science_Logo.png)](https://www.electionscience.org/)
<br>| [National Popular Vote](https://www.nationalpopularvote.com/)

### Party Members

##### Push for leaders that are pro Electoral Reform
##### Push for leaders willing to work across party lines for what is best for the country<br><sup><sup>In particular [![Labour](images/flags/GB-PARTY-LAB.svg)](Labour) ![Liberal Democrats](images/flags/GB-PARTY-LIB.svg) [![UKIP](images/svg/UKIP.svg)](Leave) [![Democrats](images/svg/Democrats.svg)](Democrats)</sup></sup>

#### [![UK](images/flags/UK.svg)](UK)

##### [![Labour](images/flags/GB-PARTY-LAB.svg)](Labour) ![Liberal Democrats](images/flags/GB-PARTY-LIB.svg) [![Green](images/svg/Green.svg)](Environment)

###### There is little point fighting the conservatives alone, the most effective way to represent your voters is to form an electoral pact for democracy and hold the election on democratising the UK

##### [![Labour](images/flags/GB-PARTY-LAB.svg)](Labour)<br>see [[Labour]] for detailed leadership candidates recommendations
###### Leader: Starmer > Nandy/Long-Bailey
###### Deputy Leader: **Murray** > Allin-Khan > Burgon/Rayner

#### [![USA](images/flags/US.svg)](USA)

##### Change at a state legislature level is quicker and builds the case for better electoral systems

### Bands

##### Use your literal voice to raise awareness
###### This is a politically neutral way to advocate change, without getting tied down to any party
##### Every movement needs a playlist<br><sup>Add relevant songs to the [[Playlist]] <br> [directly](https://gitlab.com/TheMajority/TheMajority.uk/blob/master/src/playlist.md) - [via email](mailto:playlist@themajority.uk?Subject=Song-Band)</sup>

### Graphic Designers

#### Fix the look of [the site](https://gitlab.com/TheMajority/TheMajority.uk/blob/master/src/style.css)
#### Make printable content<br><sup>Ideally with QR codes</sup>

<center>

* Stickers
* Flyers
* Posters
* Stencils

</center>


