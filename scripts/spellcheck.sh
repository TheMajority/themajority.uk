#!/usr/bin/env bash
DIR="$( dirname "${BASH_SOURCE[0]}" )/"
ASPELL_CMD="aspell --mode=html --ignore=3 --home-dir=../${DIR}"
DIST_DIR="${DIR}../dist"
ACTION=$1

ERRORS=0

check_file(){
  if [[ "$ACTION" == "update" ]] ; then
    $ASPELL_CMD -l "$2" check "$1"
  else
    OUTPUT=$($ASPELL_CMD -l "$2" list < "$1")
    if [[ "$OUTPUT" ]] ; then
        echo "=== Errors in $1"
        echo "$OUTPUT"
        ((ERRORS++))
    fi
  fi
}


pushd "$DIST_DIR" || exit 1
for filename in * ; do
  [ "$filename" != "${filename%%.*}" ] && continue
  case "$filename" in
    404|FAQ|index|Next|Licenses) check_file "$filename" en ;;
    Center|USA|Democrats|Republican|Gerrymandering) check_file "$filename" en_US ;;
    *) check_file "$filename" en_GB ;;
  esac
done

exit $ERRORS
