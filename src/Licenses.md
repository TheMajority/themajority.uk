# Licenses

### This site is available under [GPLv3+] (mostly)

* The text of the site is available under [GPLv3+]
* Most of the Licenses for images (listed below) are all [convertible to GPLv3] (or in the case of public domain do not require licensing)
    * However redistribution of `Green.svg` & `UKIP.svg` may be subject to restrictions, so are being used under [Fair Dealing],
        * if you reuse them, you may need to check the use-case complies with local laws (e.g [Fair Use] in the US)

##### Alternatives

Alternatively, if you wish to re-publish the content under a different license please feel free to [contact me](mailto:license@themajority.uk).

##### Why?

GPLv3+ was chosen as [CC-SA] was discontinued, but I'll likely be happy to license the text under any [copyleft] license

### Image Licenses

Unless otherwise states, images are public domain taken from Wikipedia

```
flags/ - Public Domain taken from Wikipedia

svg/
├── Green.svg - non-free logo - https://en.wikipedia.org/wiki/File:Green_Party_of_England_and_Wales_logo.svg
├── UKIP.svg - May not be free for use in UK - https://en.wikipedia.org/wiki/File:Logo_of_UKIP.svg
├── Reddit.svg - CC-BY-SA 4.0 - https://commons.wikimedia.org/wiki/File:Reddit_logo_orange.svg
├── gplv3.svg - Public domain - https://www.gnu.org/graphics/license-logos.en.html
├── Democrats.svg
├── Electoral-reform-society.svg
└── Spotify.svg


png/
├── The_Center_for_Election_Science_Logo.png
├── Make_Votes_Matter_logo.png - CC-BY-SA 4.0 - https://commons.wikimedia.org/wiki/File:Make_Votes_Matter_logo.png
└── Youtube.png - CC-BY-SA 4.0 - https://commons.wikimedia.org/wiki/File:Youtube_logo.png

jpg/
├── ACE_Project_Logo.jpg
└── FairVote_Logo.jpg - CC-BY-SA 4.0 - https://en.wikipedia.org/wiki/FairVote#/media/File:FairVote_Logo_2016.jpg
```

[GPLV3+]: https://gitlab.com/TheMajority/TheMajority.uk/blob/master/COPYING
[CC-SA]: https://creativecommons.org/licenses/sa/1.0/legalcode
[copyleft]: https://en.wikipedia.org/wiki/Copyleft
[convertible to GPLv3]: https://creativecommons.org/2015/10/08/cc-by-sa-4-0-now-one-way-compatible-with-gplv3/
[Fair Dealing]: https://en.wikipedia.org/wiki/Fair_dealing_in_United_Kingdom_law
[Fair Use]: https://en.wikipedia.org/wiki/Fair_use
