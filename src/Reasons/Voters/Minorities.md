# Minority voters are some of the worst affected by FPTP

## Minority votes don't matter<br><sup>by design</sup>

### [[Gerrymandering]] is often used to suppress minorities voting impact

### [Larger parties](Large) often take minority voters for granted because <u>"the other side is worse"</u><br><sup>without needling to address the needs of the group</sup>

### [Small dedicated parties](Small) can emerge under fair voting systems
#### Dedicated parties can address [issues](Single) that predominately affect minority voter, but are not a priority for [larger parties](Large)
