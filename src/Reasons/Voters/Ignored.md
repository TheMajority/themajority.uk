# Under FPTP most voters can be ignored

## Under FPTP turnout is on average 5% lower, than under fair systems

### Under FPTP most votes [don't count](https://www.makevotesmatter.org.uk/first-past-the-post#WastedVotes)
### [Large/broad/big tent parties](Large) will not represent your interests accurately
### [Small parties](Small) can't win
